#ifndef GAMBITPLUS_INCLUDE_QT_MAINWINDOW_HPP
#define GAMBITPLUS_INCLUDE_QT_MAINWINDOW_HPP

#include <QWidget>
#include <QtGui>
#include <QLayout>

#include "qt/Theme.hpp"
#include "qt/Chessboard.hpp"
#include "qt/PgnViewer.hpp"
#include "qt/KnownMoves.hpp"
#include "qt/StatusBar.hpp"

class MainWindow : public QWidget {
private:
    StatusBar* statusBar;
    Chessboard* board;
    enum state { ANALYSIS, DATABASE, REPERTOIRE } currentState;

    void paintEvent(QPaintEvent* e);
    void drawBackground(QPainter& qp);
    void keyPressEvent(QKeyEvent *event);

    void setState(state newState);

public:
    MainWindow();
};

#endif