#ifndef GAMBITPLUS_INCLUDE_QT_CHESSBOARD_HPP
#define GAMBITPLUS_INCLUDE_QT_CHESSBOARD_HPP

#include <QWidget>
#include <QtGui>
#include <QtCore>
#include <QtSvg>
#include <QObject>

#include <vector>
#include <iostream>

#include "qt/Theme.hpp"
#include "chess/MoveTree.hpp"

class Chessboard : public QWidget {

Q_OBJECT

private:
    MoveTree* moveTree;

    QPoint origin;                                  // Top left of board
    QPoint mousePos;                                // Position of mouse for dragging pieces
    int dim;                                        // Width/Height of board
    bool flipped = false;
    std::vector<std::vector<QRect>> squares;        // Rectangles denoting the positions of the board squares
    std::vector<std::string> selectedSquares {};    // Set of names of squares that are highlighted
    std::string selectedPiece = "";                 // Square of the selected piece   
    std::string heldPiece = "";

    // Generate and resize the board squares
    void generateSquares();

    // Drawing functions
    void paintEvent             (QPaintEvent* e);
    void drawBoard              (QPainter& qp);
    void drawPieces             (QPainter& qp);
    void drawSelectedSquares    (QPainter& qp);
    void drawLegalMoves         (QPainter& qp);
    void drawHeldPiece          (QPainter& qp);

    void resizeEvent(QResizeEvent* e);

    // Mouse events/helpers
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    std::string getSquare(const QPoint& pos);

public:
    Chessboard(QWidget* parent=0);
    Chessboard(MoveTree* mt, QWidget* parent=0);

    MoveTree* getMoveTree() const { return moveTree; }

public slots:
    void treeUpdate() { update(); }
};

#endif