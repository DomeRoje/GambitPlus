#ifndef GAMBITPLUS_INCLUDE_QT_KNOWNMOVES_HPP
#define GAMBITPLUS_INCLUDE_QT_KNOWNMOVES_HPP

#include <QObject>
#include <QWidget>
#include <QtGui>
#include <QFrame>
#include <QScrollArea>
#include <QLayout>

#include "qt/Theme.hpp"
#include "chess/MoveTree.hpp"
#include "qt/ControlPanel.hpp"

class KnownMoves : public QScrollArea {

Q_OBJECT

private:
    MoveTree* moveTree;

    class MoveButton : public QPushButton {
        private:
            MoveTree* moveTree;
            Move* move;
            QColor colour = Theme::WidgetBg;

            void paintEvent(QPaintEvent* e) {
                QPainter qp { this };
                qp.setRenderHint(QPainter::Antialiasing);
                qp.setPen(QPen(QColor(255, 255, 255), 0, Qt::NoPen));
                qp.setBrush(QBrush(colour, Qt::SolidPattern));
                qp.drawRect(0, 0, width(), height()); 
            }

            void leaveEvent(QEvent *event)              { colour = Theme::WidgetBg; }
            void enterEvent(QEvent *event)              { colour = Theme::Selected; }
            void mousePressEvent(QMouseEvent *event)    { moveTree->setMove(move); }
        
        public:
            MoveButton(MoveTree* mt, Move* mv) : moveTree{ mt }, move{ mv } {
                setFixedHeight(50);
                setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);

                QLabel* moveName = new QLabel { QString::fromStdString(move->getName()), this };
                moveName->setFont(QFont("Arial", 35));
                moveName->move(5, -2);

                if(!move->getNextMoves().empty()) {
                    QLabel* lineName = new QLabel { QString::fromStdString(move->getNextMoves()[0]->getLine()), this };
                    lineName->setFont(QFont("Arial", 20));
                    lineName->move(5+move->getName().length()*35, 10);
                }


            };
    };

    void populate();
    void keyPressEvent(QKeyEvent* event) { event->ignore(); }  // Release keyboard grab

public:
    KnownMoves(MoveTree* mt);

public slots:
    void treeUpdate() { populate(); update(); }
};

#endif