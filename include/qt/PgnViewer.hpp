#ifndef GAMBITPLUS_INCLUDE_QT_PGNVIEWER_HPP
#define GAMBITPLUS_INCLUDE_QT_PGNVIEWER_HPP

#include <QObject>
#include <QWidget>
#include <QtGui>
#include <QLayout>
#include <QSpacerItem>

#include "qt/Theme.hpp"
#include "qt/ControlPanel.hpp"
#include "chess/MoveTree.hpp"

class PgnViewer : public QWidget {

Q_OBJECT

private:
    MoveTree* moveTree;

    void paintEvent(QPaintEvent* e);
    void drawBackground(QPainter& qp);

public:
    PgnViewer(MoveTree* mt);

public slots:
    void treeUpdate() { update(); }
};

#endif