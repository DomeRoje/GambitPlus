#ifndef GAMBITPLUS_INCLUDE_QT_STATUSBAR_HPP
#define GAMBITPLUS_INCLUDE_QT_STATUSBAR_HPP

#include <QObject>
#include <QWidget>
#include <QtGui>
#include <QLayout>
#include <QPushButton>
#include <QLabel>

#include "qt/Theme.hpp"
#include "chess/MoveTree.hpp"

class StatusBar : public QWidget {

Q_OBJECT

private:
    MoveTree* moveTree;
    std::string state;
    QColor lineColour;

    QLabel* stateLabel  = new QLabel;
    QLabel* endLabel    = new QLabel;

    void paintEvent(QPaintEvent* e);
    void drawBackground(QPainter& qp);
    void drawPowerline(QPainter& qp);
    void updateLabels();

public:
    StatusBar(MoveTree* mt, int startState = 0);

public slots:
    void treeUpdate()   { updateLabels(); update(); }
    void stateUpdate(int newState);
};

#endif