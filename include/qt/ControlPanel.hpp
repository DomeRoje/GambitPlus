#ifndef GAMBITPLUS_INCLUDE_QT_CONTROLPANEL_HPP
#define GAMBITPLUS_INCLUDE_QT_CONTROLPANEL_HPP

#include <QObject>
#include <QWidget>
#include <QtGui>
#include <QtSvg>
#include <QPainter>
#include <QTransform>
#include <QLayout>

#include "qt/Theme.hpp"
#include "qt/Chessboard.hpp"

class ControlPanel : public QWidget {

Q_OBJECT

private:
    MoveTree* moveTree;
    
    class ControlButton : public QPushButton {
        private:
            MoveTree* moveTree;
            void (MoveTree::*moveTreeFunc)();
            QColor colour = Theme::WidgetBg;
            QSvgRenderer icon;

            void paintEvent(QPaintEvent* e) {
                QPainter qp { this };
                qp.setRenderHint(QPainter::Antialiasing);
                qp.setPen(QPen(QColor(255, 255, 255), 0, Qt::NoPen));
                qp.setBrush(QBrush(colour, Qt::SolidPattern));
                qp.drawRect(0, 0, width(), height());
                double ratio = double(icon.defaultSize().width())/double(icon.defaultSize().height());
                QRect iconRect = QRect(0, 0, ratio*height()/2, height()/2);
                QTransform A = QTransform(1, 0, 0, 1, (width()-iconRect.width())/2, (height()-iconRect.height())/2);
                icon.render(&qp, A.mapRect(iconRect));            
            }

            void leaveEvent(QEvent *event)              { colour = Theme::WidgetBg; }
            void enterEvent(QEvent *event)              { colour = Theme::Selected; }
            void mousePressEvent(QMouseEvent *event)    { std::invoke(moveTreeFunc, *moveTree); }
        
        public:
            ControlButton(MoveTree* mt, void (MoveTree::*func)(), const char* iconfile): 
            moveTree{ mt },
            moveTreeFunc{ func },
            icon{ QString(iconfile), this }
            {};
    };

public:
    ControlPanel(MoveTree* mt, QWidget* parent=0);

public slots:
    void treeUpdate();
};

#endif