#ifndef GAMBITPLUS_INCLUDE_QT_THEME_HPP
#define GAMBITPLUS_INCLUDE_QT_THEME_HPP

#include <QtGui>

namespace Theme {

static const QColor Background      = QColor(48, 140, 227);
// static const QColor Background      = QColor(243, 243, 243);
// static const QColor Background      = Qt::white;
static const QColor WidgetBg        = QColor(44, 47, 51);
static const QColor WidgetBgLight   = QColor(57, 61, 66);

static const QColor Selected        = QColor(26, 115, 199);

// Chessboard Colours
static const QColor LightSquare     = QColor(140, 162, 173);
static const QColor DarkSquare      = QColor(222, 227, 230);
static const QColor LegalSquare     = QColor(0, 0, 0);

// SVG filename variables
static const char* DoubleLeftArrow = "../data/media/icons/solid/angle-double-left.svg";
static const char* LeftArrow = "../data/media/icons/solid/angle-left.svg";
static const char* DoubleRightArrow = "../data/media/icons/solid/angle-double-right.svg";
static const char* RightArrow = "../data/media/icons/solid/angle-right.svg";

// Powerline Colours
static const QColor ALine           = QColor(119, 242, 123);
static const QColor DLine           = QColor(84, 108, 227);
static const QColor RLine           = QColor(252, 66, 109);
}

#endif