#ifndef GAMBITPLUS_INCLUDE_CHESS_MOVETREE_HPP
#define GAMBITPLUS_INCLUDE_CHESS_MOVETREE_HPP

#include <QObject>

#include "chess/Move.hpp"
#include "chess/Fen.hpp"

#include <string>

class MoveTree : public QObject {

Q_OBJECT

private:
    Move* head;
    Move* currentMove;

public:
    MoveTree();
    MoveTree(const Fen& fen);
    MoveTree(const std::string& pgn);

    std::string                 getMainLine()                               const { return head->getLine(); }
    Fen                         getCurrentFen()                             const { return currentMove->getFen(); }
    Move*                       getCurrentMove()                            const { return currentMove; }
    Move*                       getHead()                                   const { return head; }
    std::vector<Move*>          getNextMoves()                              const { return currentMove->getNextMoves(); }
    std::vector<std::string>    getLegalMoves(const std::string& square)    const { return currentMove->getLegalMoves(square); }
    char                        getPieceOn(std::string& square)             const { return currentMove->getFen().getPieceOn(square); }

    // Resetting/Loading Tree
    void loadPgn(const std::string& pgn);
    void loadFen(const Fen& fen);

    // Modifying Tree
    void addMove(const std::string& pgnmove);
    void addMove(const std::string& orig, const std::string& dest) { addMove(orig, dest, '1'); }
    void addMove(const std::string& orig, const std::string& dest, const char& promotion);
    void addMove(Move* move);
    void setMove(Move* move);
    void deleteLine(Move* move);

    // Controls
    void back()         { if(currentMove->getPrevMove() != nullptr) currentMove = currentMove->getPrevMove(); emit treeUpdate(); }
    void forward()      { if(!currentMove->getNextMoves().empty()) currentMove = currentMove->getNextMoves()[0]; emit treeUpdate(); }
    void skipBack()     { currentMove = head; emit treeUpdate(); }
    void skipForward()  { while(!currentMove->getNextMoves().empty()) currentMove = currentMove->getNextMoves()[0]; emit treeUpdate(); }
    bool canBack()      const { return head != currentMove; }
    bool canForward()   const { return currentMove->getNextMoves().size() != 0; }

signals:
    void treeUpdate();
};

#endif