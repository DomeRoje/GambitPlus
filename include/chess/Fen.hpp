#ifndef GAMBITPLUS_INCLUDE_CHESS_FEN_HPP
#define GAMBITPLUS_INCLUDE_CHESS_FEN_HPP

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>

class Fen {

using string = std::string;

private:
    string position;       // String showing piece position
    char activeColour;          // 'w' for white to play, 'b' for black to play
    string castling;       // "KQkq" to show white/black's ability to king/queenside castle
    string enpassant;      // The name of the capture square if possible. Otherwise "-"
    int halfMove;               // Moves since the last capture/pawn move
    int fullMove;               // Starts at 1, incremented after black's move

    // Return a vector of all the squares that a piece may move to ignoring checks/legality
    std::vector<string> getValidMoves(const string& square) const;

public:
    Fen();
    Fen(const string& fen);
    Fen(const string& pos, const char ac, const string& cas, const string& enp, const int hm, const int fm);

    // String representation of FEN
    friend std::ostream& operator<<(std::ostream& os, const Fen& fen);

    // Increment a FEN position by a PGN move
    friend Fen operator+=(Fen& fen, const string& move);
    friend Fen operator+(Fen& fen, const string& move);

    // Accessors
    string  getPosition()       const { return position; }
    char    getActiveColour()   const { return activeColour; }
    string  getCastling()       const { return castling; }
    string  getEnpassant()      const { return enpassant; }
    int     getHalfMove()       const { return halfMove; }
    int     getFullMove()       const { return fullMove; }

    string  getFenStr()         const;

    // Return the piece on a given square
    char getPieceOn(const string& square) const;

    // Is the active colour currently in check in the position
    bool inCheck() const;

    // Return all the squares a piece can legally move to
    std::vector<string> getLegalMoves(const string& square) const;

    void toggleActiveColour() { activeColour = (activeColour == 'w') ? 'b' : 'w'; }

    // Increment position by PGN move
    void addMove(const string& move);

    // Helper function: Is a piece the same colour as the active colour
    bool isActiveColour(const char piece) const;
};

#endif