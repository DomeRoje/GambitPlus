#ifndef GAMBITPLUS_INCLUDE_CHESS_MOVE_HPP
#define GAMBITPLUS_INCLUDE_CHESS_MOVE_HPP

#include <string>
#include <vector>
#include "chess/Fen.hpp"

class Move {
private:
    Fen fen;                        // Fen after the move has been played
    std::string name;               // Name of move. Empty string for no move
    std::vector<Move*> nextMoves;   // Vector of pointers to next moves
    Move* prevMove {};       // Pointer to the previous move

    std::string STARTFEN { "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1" };

public:
    Move();
    Move(const Fen& startfen);
    Move(const std::string& name);
    // Move(const Fen& fen);
    // Move(const Move& move) : name{ move.getName() }, fen{ move.getFen() } {}

    // Accessors
    Fen                 getFen()        const { return fen; }
    std::string         getName()       const { return name; }
    std::vector<Move*>  getNextMoves()  const { return nextMoves; }
    Move*               getPrevMove()   const { return prevMove; }

    // Return Main line starting at and including the current move
    std::string getLine()           const;
    // Return Name with move number and dots beforehand
    std::string getNumberedName()   const;

    // Return all the squares a piece can legally move to
    std::vector<std::string> getLegalMoves(const std::string& square) const { return fen.getLegalMoves(square); }

    // Link a move forward
    void addMove(Move* move);

    // Link a move backwards
    void setPrevMove(Move* move);

    // Delete the moves forward
    void clearLines() { nextMoves.clear(); }
};

#endif