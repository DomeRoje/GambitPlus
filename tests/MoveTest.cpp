#include "gtest/gtest.h"
#include "chess/MoveTree.hpp"
#include "chess/Move.hpp"
#include "chess/Fen.hpp"

#include <string>

TEST(getLine, CaroKann) {
    MoveTree moveTree {};
    moveTree.addMove("e4");
    EXPECT_EQ(moveTree.getCurrentMove()->getLine(), "1. e4");
    moveTree.addMove("c6");
    EXPECT_EQ(moveTree.getCurrentMove()->getLine(), "1... c6");
    moveTree.addMove("d4");
    EXPECT_EQ(moveTree.getCurrentMove()->getLine(), "2. d4");
}

TEST(getLine, FromWhiteFen) {
    MoveTree moveTree { Fen { "rnbqkbnr/pp2pppp/2p5/3p4/3PP3/8/PPP2PPP/RNBQKBNR w KQkq - 0 3" } };
    moveTree.addMove("exd5");
    EXPECT_EQ(moveTree.getMainLine(), "[FEN \"rnbqkbnr/pp2pppp/2p5/3p4/3PP3/8/PPP2PPP/RNBQKBNR w KQkq - 0 3\"] 3. exd5");
}

TEST(getLine, FromBlackFen) {
    MoveTree moveTree { Fen { "rnbqkbnr/pp2pppp/2p5/3P4/3P4/8/PPP2PPP/RNBQKBNR b KQkq - 0 3" } };
    moveTree.addMove("cxd5");
    EXPECT_EQ(moveTree.getMainLine(), "[FEN \"rnbqkbnr/pp2pppp/2p5/3P4/3P4/8/PPP2PPP/RNBQKBNR b KQkq - 0 3\"] 3... cxd5");
}