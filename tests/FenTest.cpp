#include "gtest/gtest.h"
#include "chess/Fen.hpp"

#include <vector>
#include <set>
#include <string>
#include <sstream>

// TEST(Constructor, String) {
//     Fen fen { "rnbqkbnr/pp2pppp/2p5/3p4/3PP3/8/PPP2PPP/RNBQKBNR w KQkq - 0 3" };
//     // std::cout << fen.getFenStr() << std::endl;
// }

TEST(getPieceOn, StartingPosition) {
    Fen fen {};

    EXPECT_EQ('p', fen.getPieceOn("a7"));
    EXPECT_EQ('r', fen.getPieceOn("h8"));
    EXPECT_EQ('n', fen.getPieceOn("g8"));
    EXPECT_EQ('b', fen.getPieceOn("f8"));
    EXPECT_EQ('k', fen.getPieceOn("e8"));
    EXPECT_EQ('q', fen.getPieceOn("d8"));

    EXPECT_EQ('P', fen.getPieceOn("e2"));
    EXPECT_EQ('R', fen.getPieceOn("a1"));
    EXPECT_EQ('N', fen.getPieceOn("g1"));
    EXPECT_EQ('B', fen.getPieceOn("f1"));
    EXPECT_EQ('Q', fen.getPieceOn("d1"));
    EXPECT_EQ('K', fen.getPieceOn("e1"));
}

TEST(getPieceOn, EvansGambit) {
    Fen fen { "r1bqk1nr/pppp1ppp/2n5/b3p3/2B1P3/2P2N2/P2P1PPP/RNBQK2R w KQkq - 1 6" };

    EXPECT_EQ('b', fen.getPieceOn("a5"));
    EXPECT_EQ('B', fen.getPieceOn("c4"));
    EXPECT_EQ('n', fen.getPieceOn("c6"));
    EXPECT_EQ('N', fen.getPieceOn("f3"));
}

TEST(inCheck, StartingPosition) {
    Fen fen {};

    EXPECT_EQ(false, fen.inCheck());
}

TEST(inCheck, CaroKann) {
    Fen fen { "rnbqkbnr/pp2pppp/8/1B1p4/3P4/8/PPP2PPP/RNBQK1NR b KQkq - 1 4" };

    EXPECT_EQ(true, fen.inCheck());
}

TEST(LegalMoves, StartingPosition) { 
    using stringVector = std::vector<std::string>;
    using stringSet = std::set<std::string>;
    
    Fen fen {};
    // Center Pawns
    stringVector legalMoves = fen.getLegalMoves("e2");
    stringSet centerPawnMoves { "e3", "e4" };
    stringSet centerPawnLegalMoves { legalMoves.begin(), legalMoves.end() };
    EXPECT_EQ(centerPawnMoves, centerPawnLegalMoves);

    // Edge Pawns
    legalMoves = fen.getLegalMoves("h2");
    stringSet flankPawnMoves { "h3", "h4" };
    stringSet flankPawnLegalMoves { legalMoves.begin(), legalMoves.end() };
    EXPECT_EQ(flankPawnMoves, flankPawnLegalMoves);

    legalMoves = fen.getLegalMoves("b1");
    stringSet knightMoves { "a3", "c3" };
    stringSet knightLegalMoves { legalMoves.begin(), legalMoves.end() };
    EXPECT_EQ(knightMoves, knightLegalMoves);
}

TEST(AddOperator, StartingPosition) {
    Fen fen{};
    fen += "e4";
    EXPECT_EQ(fen.getFenStr(), "rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq - 0 1");
    fen += "e5";
    EXPECT_EQ(fen.getFenStr(), "rnbqkbnr/pppp1ppp/8/4p3/4P3/8/PPPP1PPP/RNBQKBNR w KQkq - 0 2");
}

TEST(AddOperator, FromFen) {
    Fen fen { "rnbqkbnr/pp2pppp/2p5/3p4/3PP3/8/PPP2PPP/RNBQKBNR w KQkq - 0 3" };
    fen += "exd5";
    EXPECT_EQ(fen.getFenStr(), "rnbqkbnr/pp2pppp/2p5/3P4/3P4/8/PPP2PPP/RNBQKBNR b KQkq - 0 3");
}