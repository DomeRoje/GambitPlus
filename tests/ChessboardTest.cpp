#include <iostream>
#include <QApplication>
#include <QWidget>

#include "qt/Chessboard.hpp"
#include "chess/Fen.hpp"
#include "chess/MoveTree.hpp"

int main(int argc, char *argv[]) {
    MoveTree moveTree {};
    
    QApplication app(argc, argv);

    Chessboard window { &moveTree };

    window.setWindowTitle("Chessboard");
    window.show();

    return app.exec();
}