#include "chess/MoveTree.hpp"

MoveTree::MoveTree() {
    Move* move = new Move();    //Default move is default boardstate
    head = move;
    currentMove = head;
}

MoveTree::MoveTree(const Fen& fen) {
    Move* move = new Move(fen);
    head = move;
    currentMove = head;
}

void MoveTree::addMove(const std::string& pgnmove) {
    Move* move = new Move(pgnmove);
    move->setPrevMove(currentMove);
    currentMove->addMove(move);
    currentMove = move;
    emit treeUpdate();
}

void MoveTree::addMove(Move* move) {
    move -> setPrevMove(currentMove);
    currentMove->addMove(move);
    currentMove = move;
    emit treeUpdate();
}

void MoveTree::addMove(const std::string& orig, const std::string& dest, const char& promotion) {
    Fen fen = getCurrentFen();
    char piece = fen.getPieceOn(orig);

    std::string capture = (fen.getPieceOn(dest) == '1') ? "" : "x";

    std::string id = "";
    if(toupper(piece) != 'P') id = toupper(piece);
    else if(capture == "x") id = orig[0];

    if(toupper(piece) == 'K') {
        if(orig[0] - dest[0] == -2) {
            addMove("O-O");
            return;
        }
        else if(orig[0] - dest[0] == 2){
            addMove("O-O-O");
            return;
        }
    }

    std::string file { "" };
    std::string rank { "" };
    int i = 0;
    for(char p : fen.getPosition()) {
        if(piece == p) {
            char x = "abcdefgh"[i % 8];
            char y = (7 - (i / 8)) + 0x61;
            std::string square { x };
            square += y;
            std::vector<std::string> legalMoves = getLegalMoves(square);
            if(std::find(legalMoves.begin(), legalMoves.end(), dest) != legalMoves.end()) { // If another piece can make the same move
                if(orig[0] == square[0]) rank = orig[1];    // If they share file, specify rank
                if(orig[1] == square[1]) file = orig[0];    // If they share rank, specify file
            }
        }
    }

    std::string prom { "" };
    if(promotion != '1') {
        prom = "=";
        prom += toupper(promotion);
    }

    std::string modifier { "" };

    // Fen newfen = fen + pgn;
    // if(newfen.inCheck()) modifier = "+";
    // if(newfen.inCheckMate()) modifier = "#";

    std::string pgn = id + capture + file + rank + dest + prom + modifier;

    addMove(pgn);
}

void MoveTree::setMove(Move* move) {
    currentMove = move;
    emit treeUpdate();
}

void MoveTree::deleteLine(Move* move) {
    if(head != move) {
        currentMove = currentMove->getPrevMove();
        currentMove->clearLines();
    }
}