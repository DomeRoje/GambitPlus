#include "chess/Move.hpp"

Move::Move() {
    fen = {"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"};
    name = "";
}

Move::Move(const Fen& startfen) {
    fen = startfen;
    name = "";
}

Move::Move(const std::string& pgnname) {
    fen = Fen {"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"};
    name = pgnname;
}

void Move::addMove(Move* move) {
    for(Move* p: nextMoves) 
        if(p->getName() == move->getName())
            return;
    nextMoves.push_back(move);
}

void Move::setPrevMove(Move* move) {
    prevMove = move;
    Fen newfen = move->getFen();
    newfen += name;
    fen = newfen;
}

std::string Move::getLine() const {
    std::string line {};
    const Move* currentMove = this;
    if(getPrevMove() == nullptr) {  // If at head of tree
        if(fen.getFenStr() != STARTFEN) {
            line += "[FEN \"" + fen.getFenStr() + "\"]";
        }
    }
    else {
        line += (fen.getActiveColour() == 'w') ? std::to_string(fen.getFullMove()-1) + "... " : std::to_string(fen.getFullMove()) + ". ";
        line += name;
    }
    while(!currentMove->getNextMoves().empty()) {
        Move* activeMove = currentMove->getNextMoves()[0];
        if(line.back() == ']') {
            line += " ";
            line += (fen.getActiveColour() == 'b') ? std::to_string(fen.getFullMove()) + "... " : std::to_string(fen.getFullMove()) + ". ";
            line += name;
        }
        else {
            if(line != "") line += " ";
            line += (activeMove->getFen().getActiveColour() == 'w') ? "" : std::to_string(activeMove->getFen().getFullMove()) + ". ";
        }
        line += activeMove->getName();
        currentMove = activeMove;
    }
    return line;
}

std::string Move::getNumberedName() const {
    std::string number { (fen.getActiveColour() == 'w') ? char(fen.getFullMove() + 0x30 - 1) : char(fen.getFullMove() + 0x30) };
    std::string dots = (fen.getActiveColour() == 'w') ? "..." : ".";
    return number + dots + " " + name;
}