#include "chess/Fen.hpp"

Fen::Fen() {
    *this = Fen("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
}

Fen::Fen(const std::string& fen) {
    std::string ffen = fen;     // Copy
    std::vector<std::string::iterator> spaces;
    for(auto p = ffen.begin(); p != ffen.end(); ++p) {
        if(*p == ' ') { spaces.push_back(p); }
    }
    if(spaces.size() != 5) { std::cout << "Error: Not enough spaces" << std::endl; }    // Throw exception
    position = std::string(ffen.begin(), spaces[0]);
    activeColour = *(spaces[0] + 1);
    castling = std::string(spaces[1]+1, spaces[2]);
    enpassant = std::string(spaces[2]+1, spaces[3]);
    std::stringstream(std::string(spaces[3]+1, spaces[4])) >> halfMove;
    std::stringstream(std::string(spaces[4]+1, ffen.end())) >> fullMove;
}

Fen::Fen(const std::string& pos, const char ac, const std::string& cas, const std::string& enp, const int hm, const int fm) {
    position = pos;
    activeColour = ac;
    castling = cas;
    enpassant = enp;
    halfMove = hm;
    fullMove = fm;
}

char Fen::getPieceOn(const std::string& square) const {
    int loc = 8*(8 - (int(square[1]) - 0x30)) + int(square[0]) - 0x61;  //a8 -> 0, h8 -> 7, h1 -> 63
    int pos = 0;
    for(auto p: position) {
        bool piece = false;
        if(std::string("prnbkqPRNBKQ").find(p) != std::string::npos) {
            ++pos;  // If piece, increment by 1
            piece = true;
        }
        else if(p != '/') {
            pos += int(p) - 0x30;
            piece = false;
        }
        if(pos > loc) {
            if(piece) { return p; }
            else { return '1'; }
        }
    }
    return '1';     // Just in case
}

bool Fen::isActiveColour(const char piece) const {
    if(activeColour == 'w') return toupper(piece) == piece;
    else return tolower(piece) == piece;
}

std::ostream& operator<<(std::ostream& os, const Fen& fen) {
    os << fen.getPosition() << " ";
    os << fen.getActiveColour() << " ";
    os << fen.getCastling() << " ";
    os << fen.getEnpassant() << " ";
    os << fen.getHalfMove() << " ";
    os << fen.getFullMove();
    return os;
}

Fen operator+(Fen& fen, const std::string& move) {
    char piece {};
    if(std::string("RNBKQ").find(move[0]) != std::string::npos) piece = (fen.getActiveColour() == 'w') ? move[0] : tolower(move[0]);
    else if(move[0] == 'O') piece = (fen.getActiveColour() == 'w') ? 'K' : 'k';
    else piece = (fen.getActiveColour() == 'w') ? 'P' : 'p';

    bool capture = move.find('x') != std::string::npos;

    char activeColour = (fen.getActiveColour() == 'w') ? 'b' : 'w';
    std::string castling = fen.getCastling();
    std::string enpassant = "-";
    int halfMove = (toupper(piece) == 'P' || capture == true) ? 0 : fen.getHalfMove() + 1;
    int fullMove = (activeColour == 'b') ? fen.getFullMove() : fen.getFullMove() + 1;

    // Arange position into 2D char array
    std::vector<std::vector<char>> pos;
    std::vector<char> rank;
    for(char p : fen.getPosition()) {
        if(p == '/') {
            pos.push_back(rank);
            rank.clear();
        }
        else if(isdigit(p)) for(int i = 0; i < p - 0x30; i++) rank.push_back('1');
        else rank.push_back(p);
    }
    pos.push_back(rank);

    // Strip move of check modifiers
    std::string newmove = (move.back() == '#' || move.back() == '+') ? std::string(move.begin(), move.end()-1) : move;

    // Arange pieces in position
    if(newmove == "O-O") {
        int backrank = (activeColour == 'w') ? 0 : 7;
        char queen = (activeColour == 'w') ? 'q' : 'Q';
        char king = pos[backrank][4];
        char rook = pos[backrank][7];
        pos[backrank][4] = '1';
        pos[backrank][5] = rook;
        pos[backrank][6] = king;
        pos[backrank][7] = '1';
        castling.erase(std::remove(castling.begin(), castling.end(), king), castling.end());
        castling.erase(std::remove(castling.begin(), castling.end(), queen), castling.end());
    }
    else if(newmove == "O-O-O") {
        int backrank = (activeColour == 'w') ? 0 : 7;
        char queen = (activeColour == 'w') ? 'q' : 'Q';
        char king = pos[backrank][4];
        char rook = pos[backrank][0];
        pos[backrank][4] = '1';
        pos[backrank][3] = rook;
        pos[backrank][2] = king;
        pos[backrank][1] = '1';
        castling.erase(std::remove(castling.begin(), castling.end(), king), castling.end());
        castling.erase(std::remove(castling.begin(), castling.end(), queen), castling.end());
    }
    else {
        std::string dest = std::string(newmove.end()-2, newmove.end());
        std::string orig;
        std::string file;
        std::string rank;
        if(capture) newmove = std::string(newmove.begin(), newmove.end()-3);
        else newmove = std::string(newmove.begin(), newmove.end()-2);
        if(newmove.size() != 0) {
            if(std::string("RBNKQ").find(toupper(newmove[0])) != std::string::npos) newmove = std::string(newmove.begin()+1, newmove.end());
        }
        if(newmove.size() == 2) {
            file = newmove[0];
            rank = newmove[1];
        }
        if(newmove.size() == 1) {
            if(isdigit(newmove[0])) rank = newmove;
            else file = newmove;
        }

        std::string yrange = (rank.empty()) ? "12345678" : std::string(rank);
        std::string xrange = (file.empty()) ? "abcdefgh" : std::string(file);
        bool found = false;
        for(char x : xrange) {
            for(char y : yrange) {
                std::string square { x };
                square += y;
                if(fen.getPieceOn(square) == piece) {
                    std::vector<std::string> legalMoves = fen.getLegalMoves(square);
                    if(find(legalMoves.begin(), legalMoves.end(), dest) != legalMoves.end()) {
                        orig = square;
                        found = true;
                        break;
                    }
                }
            }
            if(found) break;
        }

        int ox = orig[0] - 0x61;
        int oy = 8 - (orig[1] - 0x30);
        int dx = dest[0] - 0x61;
        int dy = 8 - (dest[1] - 0x30);

        if(toupper(piece) == 'P') {
            if(abs(dy - oy) == 2) {
                char opawn = (activeColour == 'w') ? 'P' : 'p';
                if( 0 <= dx - 1 && dx - 1 < 8) {
                    if(pos[dy][dx-1] == opawn) {
                        enpassant = dest[0];
                        enpassant += (dest[1]+orig[1])/2;
                    }
                }
                if( 0 <= dx + 1 && dx + 1 < 8) {
                    if(pos[dy][dx+1] == opawn) {
                        enpassant = dest[0];
                        enpassant += (dest[1]+orig[1])/2;
                    }
                }
            }
            else if(dest == fen.getEnpassant()) {
                pos[oy][dx] = '1';
            }
        }

        pos[oy][ox] = '1';
        pos[dy][dx] = piece;
    }

    // flatten 2D char array into fen string
    std::string fenstr;
    for(auto rank : pos) {
        if(fenstr != "") fenstr += "/";

        int i = 0;
        for(auto p : rank) {
            if(p == '1') {
                i++;
            }
            else {
                if(i != 0) fenstr += i+0x30;
                i = 0;
                fenstr += p;
            }
        }
        if(i != 0) fenstr += i+0x30;
    }

    Fen newfen { fenstr,
                 activeColour,
                 castling,
                 enpassant,
                 halfMove,
                 fullMove
                };
    
    return newfen;
}

Fen operator+=(Fen& fen, const std::string& move) {
    fen = fen + move;
    return fen;
}

bool Fen::inCheck() const {
    std::string fen = position;
    char king = (activeColour == 'w') ? 'K' : 'k';
    
    // Find position of king
    int rank = 7;
    int file = 0;
    for(auto p : position) {
        if(p == '/') {
            rank--;
            file = 0;
        }
        else if(p == king) break;
        else if(isdigit(p)) file += (p - 0x30);
        else file++;
    }
    std::string kingPos;
    kingPos += "abcdefgh"[file];
    kingPos += rank + 0x31;

    // Iterate through pieces
    rank = 7;
    file = 0;
    for(auto p : position) {
        if(p == '/') {
            rank--;
            file = 0;
        }
        else if(isdigit(p)) file += (p - 0x30);
        else if(!isActiveColour(p)) {   // Else p is an opponent piece
            std::string piecePos;       // Position of piece
            piecePos += "abcdefgh"[file];
            piecePos += rank + 0x31;
            Fen newfen { *this };            // Copy fen but change active colour
            newfen.toggleActiveColour();    // Change active colour
            std::vector<std::string> validMoves = newfen.getValidMoves(piecePos);  // Get the valid moves of opponent piece
            for(auto move : validMoves) {           // Iterate through valid moves
                if(move == kingPos) return true;    // If opponent piece can move onto king then king is in check;
            }
            file++;
        }
        else file++;
    }
    return false;
}

std::vector<std::string> Fen::getValidMoves(const std::string& square) const {
    std::vector<std::string> validMoves = {};       // Empty vector of legal squares
    char piece = getPieceOn(square);                // Name of piece on square
    if(!isActiveColour(piece)) return validMoves;   // A piece that is not active has no valid moves
    char upiece = toupper(piece);                   // Uppercase name of piece

    if(upiece == 'R' || upiece == 'Q') {            // If Rook or Queen
        std::vector<std::pair<int, int>> move { {1, 0}, {-1, 0}, {0, 1}, {0, -1}};
        for(auto p : move) {
            int x = std::string("abcdefgh").find(square[0]);
            int y = square[1] - 0x31;
            x += p.first;
            y += p.second;
            while(0 <= x && x <= 7 && 0 <= y && y <= 7) {
                std::string newsquare;
                newsquare += "abcdefgh"[x];
                newsquare += y + 0x31;
                char newpiece = getPieceOn(newsquare);
                if(newpiece == '1') validMoves.push_back(newsquare);
                else if(isActiveColour(newpiece)) break;
                else {
                    validMoves.push_back(newsquare);
                    break;
                }
                x += p.first;
                y += p.second;
            }
        }
    }
    if(upiece == 'B' || upiece == 'Q') {            // If Bishop or Queen
        std::vector<std::pair<int, int>> move { {1, 1}, {-1, -1}, {-1, 1}, {1, -1}};
        for(auto p : move) {
            int x = std::string("abcdefgh").find(square[0]);
            int y = square[1] - 0x31;
            x += p.first;
            y += p.second;
            while(0 <= x && x <= 7 && 0 <= y && y <= 7) {
                std::string newsquare;
                newsquare += "abcdefgh"[x];
                newsquare += y + 0x31;
                char newpiece = getPieceOn(newsquare);
                if(newpiece == '1') validMoves.push_back(newsquare);
                else if(isActiveColour(newpiece)) break;
                else {
                    validMoves.push_back(newsquare);
                    break;
                }
                x += p.first;
                y += p.second;
            }
        }
    }
    else if(upiece == 'P') {                        // If Pawn
        char backrank = (activeColour == 'w') ? '2' : '7';
        int dir = (activeColour == 'w') ? 1 : -1;

        std::vector<std::pair<int, int>> move { {1, dir}, {-1, dir} };  // Captures
        for(auto p : move) {
            int x = std::string("abcdefgh").find(square[0]);
            int y = square[1] - 0x31;
            x += p.first;
            y += p.second;
            if(0 <= x && x <= 7 && 0 <= y && y <= 7) {
                std::string newsquare;
                newsquare += "abcdefgh"[x];
                newsquare += y + 0x31;
                char newpiece = getPieceOn(newsquare);
                if((newpiece != '1' && !isActiveColour(newpiece)) || newsquare == enpassant) validMoves.push_back(newsquare);
            }
        }
        
        // No out-of-bound checks!
        int x = std::string("abcdefgh").find(square[0]);
        int y = square[1] - 0x31;
        y += dir;   // Push
        std::string newsquare;
        newsquare += "abcdefgh"[x];
        newsquare += y + 0x31;
        if(0 <= x && x <= 7 && 0 <= y && y <= 7) {
            char newpiece = getPieceOn(newsquare);
            if(newpiece == '1') {
                validMoves.push_back(newsquare);
                if(square[1] == backrank) {
                    y += dir;   // Double push
                    newsquare = "";
                    newsquare += "abcdefgh"[x];
                    newsquare += y + 0x31;
                    newpiece = getPieceOn(newsquare);
                    if(newpiece == '1') validMoves.push_back(newsquare);
                }
            }
        }
    }
    else if(upiece == 'N') {                        // If Knight
        std::vector<std::pair<int, int>> move { {1, 2}, {2, 1}, {-1, -2}, {-2, -1}, {1, -2}, {-2, 1}, {-1, 2}, {2, -1}};
        for(auto p : move) {
            int x = std::string("abcdefgh").find(square[0]);
            int y = square[1] - 0x31;
            x += p.first;
            y += p.second;
            if(0 <= x && x <= 7 && 0 <= y && y <= 7) {
                std::string newsquare;
                newsquare += "abcdefgh"[x];
                newsquare += y + 0x31;
                char newpiece = getPieceOn(newsquare);
                if(newpiece == '1') validMoves.push_back(newsquare);
                else if(!isActiveColour(newpiece)) validMoves.push_back(newsquare);
            }
        }
    }
    else if(upiece == 'K') {                        // If King
        // Regular King moves
        std::vector<std::pair<int, int>> move { {1, 0}, {0, 1}, {-1, 0}, {0, -1}, {1, 1}, {-1, 1}, {1, -1}, {-1, -1}};
        for(auto p : move) {
            int x = std::string("abcdefgh").find(square[0]);
            int y = square[1] - 0x31;
            x += p.first;
            y += p.second;
            if(0 <= x && x <= 7 && 0 <= y && y <= 7) {
                std::string newsquare;
                newsquare += "abcdefgh"[x];
                newsquare += y + 0x31;
                char newpiece = getPieceOn(newsquare);
                if(newpiece == '1') validMoves.push_back(newsquare);
                else if(!isActiveColour(newpiece)) validMoves.push_back(newsquare);
            }
        }

        // Castling
        char king = piece;
        char queen = (activeColour == 'w') ? 'Q' : 'q';
        char backrank = (activeColour == 'w') ? '1' : '8';
        if(castling.find(king) != std::string::npos) {  // If king has not forfeited kingside castle
            // Check if the squares on the f & g file next to the king are unoccupied
            std::string castlingsquare { "f" };
            castlingsquare += backrank;
            bool fsquare = getPieceOn(castlingsquare) == '1';
            castlingsquare = "g";
            castlingsquare += backrank;
            bool gsquare = getPieceOn(castlingsquare) == '1';
            if(fsquare && gsquare) validMoves.push_back(castlingsquare);
        }
        if(castling.find(queen) != std::string::npos) {  // If king has not forfeited queenside castle
            // Check if the squares on the d & b & c file next to the king are unoccupied
            std::string castlingsquare { "d" };
            castlingsquare += backrank;
            bool dsquare = getPieceOn(castlingsquare) == '1';
            castlingsquare = "b";
            castlingsquare += backrank;
            bool bsquare = getPieceOn(castlingsquare) == '1';
            castlingsquare = "c";
            castlingsquare += backrank;
            bool csquare = getPieceOn(castlingsquare) == '1';
            if(bsquare && dsquare && csquare) validMoves.push_back(castlingsquare);
        }
    }
    return validMoves;
}

std::vector<std::string> Fen::getLegalMoves(const std::string& square) const {
    std::vector<std::string> legalMoves;
    char piece = getPieceOn(square);
    char upiece = toupper(piece);

    for(std::string move : getValidMoves(square)) {
        std::vector<std::vector<std::string>> pos;
        if(upiece == 'P' && square == enpassant) {  // En passant

        }
        else if(upiece == 'K' && (square[0] - move[0] == 2 || square[0] - move[0])) {   // If king moves two squares i.e castling

        }
        else {  // Regular move

        }
    }
    return getValidMoves(square);   // Default. Delete when function is done
}

std::string Fen::getFenStr() const {
    std::stringstream inp;
    inp << *this;
    std::string out;
    inp >> out;
    for(int i = 0; i < 5; i++) {
        std::string temp;
        inp >> temp;
        out += " " + temp;
    }
    return out;
}