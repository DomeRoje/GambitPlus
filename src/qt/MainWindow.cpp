#include "qt/MainWindow.hpp"

MainWindow::MainWindow() {
    board = new Chessboard;
    statusBar = new StatusBar{ board->getMoveTree() };

    setState(ANALYSIS);

    QGridLayout* winLayout = new QGridLayout;
    winLayout->addWidget(new KnownMoves{ board->getMoveTree() }, 0, 0, 3, 1);

    QVBoxLayout* boardArea = new QVBoxLayout;
    boardArea->addWidget(board);
    boardArea->addWidget(statusBar);
    winLayout->addItem(boardArea, 0, 1, 3, 2);

    winLayout->addWidget(new PgnViewer{ board->getMoveTree() }, 0, 3, 3, 1);

    setLayout(winLayout);
    layout()->setMargin(0);
    layout()->setContentsMargins(0, 0, 0, 0);
    layout()->setSpacing(0);
}

void MainWindow::paintEvent(QPaintEvent* e) {
    QPainter qp(this);

    drawBackground(qp);
}

void MainWindow::drawBackground(QPainter& qp) {
    qp.setPen(QPen(QColor(255, 255, 255), 0, Qt::NoPen));
    qp.setBrush(QBrush(Theme::Background, Qt::SolidPattern));
    qp.drawRect(0, 0, width(), height());
}

void MainWindow::setState(state newState) {
    currentState = newState;
    statusBar->stateUpdate(currentState);
}

void MainWindow::keyPressEvent(QKeyEvent *event) {
    switch(event->key()) {
        case(Qt::Key_Left):
            board->getMoveTree()->back();
            break;
        
        case(Qt::Key_Right):
            board->getMoveTree()->forward();
            break;

        case(Qt::Key_Up):
            board->getMoveTree()->skipBack();
            break;
        
        case(Qt::Key_Down):
            board->getMoveTree()->skipForward();
            break;

        case(Qt::Key_A):
            if(event->modifiers() == Qt::ControlModifier) setState(ANALYSIS);
            break;

        case(Qt::Key_D):
            if(event->modifiers() == Qt::ControlModifier) setState(DATABASE);
            break;

        case(Qt::Key_R):
            if(event->modifiers() == Qt::ControlModifier) setState(REPERTOIRE);
            break;

        default:
            event->ignore();
            break;
    }
}