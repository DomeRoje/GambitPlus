#include "qt/ControlPanel.hpp"

ControlPanel::ControlPanel(MoveTree* mt, QWidget* parent) : QWidget(parent) {
    moveTree = mt;

    QObject::connect(moveTree, &MoveTree::treeUpdate,
                     this,     &ControlPanel::treeUpdate);

    setLayout(new QHBoxLayout);
    layout()->setMargin(0);
    layout()->setContentsMargins(0, 0, 0, 0);
    layout()->setSpacing(0);
    setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);

    layout()->addWidget(new ControlButton { moveTree, &MoveTree::skipBack, Theme::DoubleLeftArrow });
    layout()->addWidget(new ControlButton { moveTree, &MoveTree::back, Theme::LeftArrow });
    layout()->addWidget(new ControlButton { moveTree, &MoveTree::forward, Theme::RightArrow });
    layout()->addWidget(new ControlButton { moveTree, &MoveTree::skipForward, Theme::DoubleRightArrow });
}

void ControlPanel::treeUpdate() {
    update();
    // Update buttons
}