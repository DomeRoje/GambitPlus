#include "qt/PgnViewer.hpp"

PgnViewer::PgnViewer(MoveTree* mt) : moveTree{ mt } {
    setLayout(new QVBoxLayout);
    layout()->setMargin(0);
    layout()->setContentsMargins(0, 0, 0, 0);
    layout()->setSpacing(0);

    layout()->addItem(new QSpacerItem{ 0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding });
    layout()->addWidget(new ControlPanel { moveTree, this });
}

void PgnViewer::paintEvent(QPaintEvent* e) {
    QPainter qp { this };
    drawBackground(qp);   
}

void PgnViewer::drawBackground(QPainter& qp) {
    qp.setPen(QPen(QColor(255, 255, 255), 0, Qt::NoPen));
    qp.setBrush(QBrush(Theme::WidgetBgLight, Qt::SolidPattern));
    qp.drawRect(0, 0, width(), height());   
}