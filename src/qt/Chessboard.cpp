#include "qt/Chessboard.hpp"

Chessboard::Chessboard(QWidget* parent) : QWidget(parent) {
    generateSquares();
    moveTree = new MoveTree();
    QObject::connect(moveTree, &MoveTree::treeUpdate,
                     this,     &Chessboard::treeUpdate);
}

Chessboard::Chessboard(MoveTree* mt, QWidget* parent) : QWidget(parent) {
    generateSquares();
    moveTree = mt;
    QObject::connect(moveTree, &MoveTree::treeUpdate,
                     this,     &Chessboard::treeUpdate);
}

void Chessboard::paintEvent(QPaintEvent* e) {
    QPainter qp(this);
    qp.setRenderHint(QPainter::Antialiasing);
    drawBoard(qp);
    drawSelectedSquares(qp);
    drawLegalMoves(qp);
    drawPieces(qp);
    drawHeldPiece(qp);
}

void Chessboard::drawBoard(QPainter& qp) {
    qp.setPen(QPen(QColor(255, 255, 255), 0, Qt::NoPen));
    for(auto y = 0; y <=7; ++y) {
        for(auto x = 0; x <= 7; ++x) {
            if((x+y) % 2 == 0) qp.setBrush(QBrush(Theme::DarkSquare, Qt::SolidPattern));
            else qp.setBrush(QBrush(Theme::LightSquare, Qt::SolidPattern));
            qp.drawRect(squares[y][x]);
        }
    }
}

void Chessboard::drawSelectedSquares(QPainter& qp) {
    for(auto p : selectedSquares) {
        int x = (flipped) ? 7 - (p[0] - 0x61) : p[0] - 0x61;
        int y = (flipped) ? p[1] - 0x31 : 7 - (p[1] - 0x31);
        QColor colour { 255, 0, 0 };
        colour.setAlphaF(0.5);
        qp.setBrush(QBrush(colour, Qt::SolidPattern));
        qp.drawRect(squares[y][x]);
    }
}

void Chessboard::drawLegalMoves(QPainter& qp) {
    if(selectedPiece != "") {
        for(auto p : moveTree->getLegalMoves(selectedPiece)) {
            int x = (flipped) ? 7 - (p[0] - 0x61) : p[0] - 0x61;
            int y = (flipped) ? p[1] - 0x31 : 7 - (p[1] - 0x31);
            QColor colour = Theme::LegalSquare;
            colour.setAlphaF(0.5);
            qp.setBrush(QBrush(colour, Qt::SolidPattern));
            if(moveTree->getCurrentFen().getPieceOn(p) == '1') {
                QRect ellipse = squares[y][x];
                QTransform A { 1, 0, 0, 1, (double)-ellipse.center().x(), (double)-ellipse.center().y() };
                QTransform B { 0.3, 0, 0, 0.3, (double)ellipse.center().x(), (double)ellipse.center().y() };
                qp.drawEllipse((A*B).mapRect(ellipse));
            }
            else {
                int tabWidth = dim/32;
                QPoint corners[4] = {
                    squares[y][x].topLeft(),
                    squares[y][x].topLeft() + QPoint(squares[y][x].width(), 0),
                    squares[y][x].topLeft() + QPoint(0, squares[y][x].height()),
                    squares[y][x].topLeft() + QPoint(squares[y][x].width(), squares[y][x].height())
                };
                for(int i = 0; i < 4; i++) {
                    int xparity = (i % 2 == 1) ? -1 : 1;
                    int yparity = (i < 2) ? 1 : -1;
                    QPoint triangle[3];
                    triangle[0] = corners[i];
                    triangle[1] = corners[i] + QPoint(0, yparity*tabWidth);
                    triangle[2] = corners[i] + QPoint(xparity*tabWidth, 0);
                    qp.drawPolygon(triangle, 3);
                }
            }
        }
    }
}

void Chessboard::drawPieces(QPainter& qp) {
    std::string position = moveTree->getCurrentFen().getPosition();
    int i = 0;
    for(auto p : position) {
        if(isdigit(p)) i += p-0x30;
        else if(p != '/') {
            int x = (flipped) ? 7 - (i%8) : i%8;
            int y = (flipped) ? 7 - (i/8) : i/8;
            char name = tolower(p);
            QString piece;
            piece += (toupper(p) == p) ? 'w' : 'b';
            piece += name;
            QString svgfile = "../data/media/pieces/Alpha/" + piece + ".svg";
            QSvgRenderer renderer { QString(svgfile), this };
            renderer.render(&qp, squares[y][x]);
            ++i;
        }
    }
}

void Chessboard::drawHeldPiece(QPainter& qp) {
    if(heldPiece != "") {
        // Draw transparent piece
        int x = (flipped) ? 7 - (heldPiece[0] - 0x61) : heldPiece[0] - 0x61;
        int y = (flipped) ? heldPiece[1] - 0x31 : 7 - (heldPiece[1] - 0x31);
        QColor colour = ((x + y) % 2 == 0) ? Theme::DarkSquare : Theme::LightSquare;
        colour.setAlphaF(0.5);
        qp.setBrush(QBrush(colour, Qt::SolidPattern));
        qp.drawRect(squares[y][x]);

        // Draw piece at mouse
        char p = moveTree->getPieceOn(heldPiece);
        char name = tolower(p);
        QString piece;
        piece += (toupper(p) == p) ? 'w' : 'b';
        piece += name;
        QString svgfile = "../data/media/pieces/Alpha/" + piece + ".svg";
        QSvgRenderer renderer { QString(svgfile), this };
        renderer.render(&qp, QRect(mousePos.x() - dim/16, mousePos.y() - dim/16, dim/8, dim/8));
    }
}

std::string Chessboard::getSquare(const QPoint& pos) {
    QPoint rel = pos - origin;
    if(rel.x() < 0 || rel.x() > dim || rel.y() < 0 || rel.y() > dim) return "";     // If not on board return empty string
    int x = rel.x()/(dim/8);
    int y = rel.y()/(dim/8);
    std::string square { (flipped) ? "hgfedcba"[x] : "abcdefgh"[x] };
    square += (flipped) ? y + 0x31 : (8 - y) + 0x30;
    return square;
}

void Chessboard::mousePressEvent(QMouseEvent *event) {
    std::string square = getSquare(event->pos());
    if(square == "") return;

    switch(event->button()) {
        case(Qt::RightButton): {
            // Toggle Selected Squares
            auto p = std::find(selectedSquares.begin(), selectedSquares.end(), square);     // Find square in selected squares
            if (p == selectedSquares.end())  selectedSquares.push_back(square);             // Add if not in vector
            else selectedSquares.erase(p);                                                  // Add otherwise

            // Deselect piece
            selectedPiece = "";
            heldPiece = "";

            break;
        }
        case(Qt::LeftButton): {
            selectedSquares.clear();

            if(selectedPiece != "") {
                std::vector<std::string> legalMoves = moveTree->getLegalMoves(selectedPiece);
                auto p = std::find(legalMoves.begin(), legalMoves.end(), square);     // Find square in legal moves
                if (p != legalMoves.end())  moveTree->addMove(selectedPiece, square);
            }

            // Select pieces of active colour
            if(moveTree->getPieceOn(square) != '1' && moveTree->getCurrentFen().isActiveColour(moveTree->getPieceOn(square))) {
                selectedPiece = square;
                mousePos = event->pos();
            }
            else selectedPiece = "";
            heldPiece = selectedPiece;
            break;
        }
        default:
            break;
    }
    update();
}

void Chessboard::mouseReleaseEvent(QMouseEvent *event) {
    std::string square = getSquare(event->pos());
    if(square == "") return;

    switch(event->button()) {
        case(Qt::RightButton): {
            break;
        }
        case(Qt::LeftButton): {
            if(selectedPiece != "") {
                heldPiece = "";
                if(square != selectedPiece) {   // If released onto a different square
                    std::vector<std::string> legalMoves = moveTree->getLegalMoves(selectedPiece);
                    auto p = std::find(legalMoves.begin(), legalMoves.end(), square);     // Find square in legal moves
                    if (p != legalMoves.end())  moveTree->addMove(selectedPiece, square);
                    selectedPiece = "";
                }
            }
            break;
        }
        default:
            break;
    }
    update();
}

void Chessboard::mouseMoveEvent(QMouseEvent *event) {
    mousePos = event->pos();
    update();
}

void Chessboard::resizeEvent(QResizeEvent* e) {
    generateSquares();
}

void Chessboard::generateSquares() {
    dim = std::min(width(), height());
    int squaredim = dim/8;
    // origin = QPoint((width() - 8*(dim/8))/2, 0);                         // Center Horizontally
    // origin = QPoint(0, (height() - 8*(dim/8))/2);                        // Center Vertically
    origin = QPoint((width() - 8*(dim/8))/2, (height() - 8*(dim/8))/2);     // Center

    squares.clear();
    for(auto y = 0; y <=7; ++y) {
        std::vector<QRect> rank;
        for(auto x = 0; x <= 7; ++x) {
            QPoint topLeft = origin + QPoint(x*squaredim, y*squaredim);
            QRect rect = QRect(topLeft.x(), topLeft.y(), squaredim, squaredim);
            rank.push_back(rect);
        }
        squares.push_back(rank);
    }
}