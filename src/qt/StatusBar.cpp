#include "qt/StatusBar.hpp"

StatusBar::StatusBar(MoveTree* mt, int startState) : moveTree{ mt } {
    QFont font = QFont("Arial", 10);
    font.setBold(true);
    stateLabel->setFont(font);
    endLabel->setFont(font);

    QObject::connect(moveTree, &MoveTree::treeUpdate,
                     this,     &StatusBar::treeUpdate);
    
    stateUpdate(startState);

    QHBoxLayout* barLayout = new QHBoxLayout;
    setLayout(barLayout);
    layout()->setMargin(0);
    layout()->setContentsMargins(10, 0, 10, 0);
    layout()->setSpacing(0);
    setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    setFixedHeight(20);

    layout()->addWidget(stateLabel);
    layout()->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum));
    layout()->addWidget(endLabel);
    updateLabels();
}

void StatusBar::paintEvent(QPaintEvent* e) {
        QPainter qp { this };
        qp.setRenderHint(QPainter::Antialiasing);
        drawBackground(qp);
        drawPowerline(qp);
}

void StatusBar::drawBackground(QPainter& qp) {
    qp.setPen(QPen(QColor(255, 255, 255), 0, Qt::NoPen));
    qp.setBrush(QBrush(Theme::WidgetBgLight, Qt::SolidPattern));
    qp.drawRect(0, 0, width(), height()); 
}

void StatusBar::drawPowerline(QPainter& qp) {
    qp.setPen(QPen(QColor(255, 255, 255), 0, Qt::NoPen));
    qp.setBrush(QBrush(lineColour, Qt::SolidPattern));

    int w = 20 + stateLabel->width();
    QRect bound = QRect {0, 0, w, height()};
    qp.drawRect(bound);
    QPoint triangle[3] = {  QPoint(w, 0),
                            QPoint(w + height()/2, height()/2),
                            QPoint(w, height())
    };
    qp.drawPolygon(triangle, 3);

    qp.setPen(QPen(QColor(255, 255, 255), 0, Qt::SolidLine));
    // qp.drawText(bound, 132, QString::fromStdString(state));
}

void StatusBar::updateLabels() {
    endLabel->setText(QString::fromStdString(moveTree->getMainLine()));
    stateLabel->setText(QString::fromStdString(state));
}

void StatusBar::stateUpdate(int newState) {
    switch(newState) {
        case(0):
            state = "Analysis";
            lineColour = Theme::ALine;
            break;
        case(1):
            state = "Database";
            lineColour = Theme::DLine;
            break;
        case(2):
            state = "Repertoire";
            lineColour = Theme::RLine;
            break;
        default:
            state = "Unkown";
    }
    updateLabels();
    update();
}