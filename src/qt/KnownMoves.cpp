#include "qt/KnownMoves.hpp"

KnownMoves::KnownMoves(MoveTree* mt) : moveTree{ mt } {
    QObject::connect(moveTree, &MoveTree::treeUpdate,
                     this,     &KnownMoves::treeUpdate);

    setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    setFrameShape(QFrame::NoFrame);
    setPalette(QPalette(Theme::WidgetBg, Theme::WidgetBgLight));
    setWidgetResizable(true);
}

void KnownMoves::populate() {
    // Create layout
    QVBoxLayout* widgetLayout = new QVBoxLayout;
    widgetLayout->setMargin(0);
    widgetLayout->setContentsMargins(0, 0, 0, 0);
    widgetLayout->setSpacing(0);

    QWidget* widget = new QWidget;
    
    for(Move* p : moveTree->getCurrentMove()->getNextMoves()) {
        MoveButton* button = new MoveButton { moveTree, p };
        widgetLayout->addWidget(button);
    }

    widget->setLayout(widgetLayout);
    widget->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
    setWidget(widget); 
}