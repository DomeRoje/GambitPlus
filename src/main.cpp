#include <iostream>
#include <QApplication>
#include <QWidget>

#include "qt/MainWindow.hpp"

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);

    MainWindow window;

    // window.resize(250, 150);
    window.setWindowTitle("GambitPlus");
    window.show();

    return app.exec();
}